<?php

use yii\db\Migration;

/**
 * Handles the creation of table `articles`.
 */
class m170730_195346_create_articles_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('articles', [
            'id' => $this->primaryKey(),
            'page_img_path' => $this->string(255),
            'page_alias' => $this->string(255),
            'page_category' => $this->string(255),
            'page_title' => $this->string(255)->notNull(),
            'page_meta_d' => $this->text()->notNull(),
            'page_meta_k' => $this->text(),
            'page_content' => $this->text()->notNull(),
            'page_publish' => $this->string(1)->defaultValue('Y')->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('articles');
    }
}
