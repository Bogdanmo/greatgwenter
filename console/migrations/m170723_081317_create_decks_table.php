<?php

use yii\db\Migration;

class m170723_081317_create_decks_table extends Migration
{
    public function up()
    {
        $this->createTable('decks', [
            'id' => $this->primaryKey(),
            'deck_faction' => $this->string(255)->notNull(),
            'deck_name' => $this->string(255)->notNull(),
            'deck_img_path' => $this->string(255)->notNull(),
            'deck_version' => $this->string(255),
            'deck_description' => $this->text(),
            'deck_strategy' => $this->text()->notNull(),
            'deck_publish' => $this->string(1)->defaultValue('Y')->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('news');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170723_081317_decks cannot be reverted.\n";

        return false;
    }
    */
}
