<?php
namespace common\models;


use yii\web\UploadedFile;

class UploadForm extends \yii\db\ActiveRecord
{
    /**
     * @var UploadedFile
     */
    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            global $name;
            $name = 'uploads/' . date("U.d.m.y") . '.' . $this->imageFile->extension;
            $this->imageFile->saveAs($name);
            $fp = fopen('files/uploaded_images.txt', 'a');
            $test = fwrite($fp, $name. "\r\n");
            if ($test) echo 'Данные в файл успешно занесены.';
            else echo 'Ошибка при записи в файл.';
            fclose($fp);
            echo $name . '</br>';
            return true;
        } else {
            return false;
        }
    }
}
/**
 * Created by PhpStorm.
 * User: Админ
 * Date: 31.07.2017
 * Time: 17:17
 */