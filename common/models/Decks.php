<?php

namespace common\models;

use Yii;
use yii\helpers\BaseUrl;
use yii\web\UploadedFile;

/**
 * This is the model class for table "decks".
 *
 * @property integer $id
 * @property string $deck_faction
 * @property string $deck_name
 * @property string $deck_img_path
 * @property string $deck_version
 * @property string $deck_description
 * @property string $deck_strategy
 * @property string $deck_publish
 */
class Decks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'decks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['deck_faction', 'deck_name', 'deck_strategy'], 'required'],
            [['deck_description', 'deck_strategy'], 'string'],
            [['deck_faction', 'deck_name', 'deck_img_path', 'deck_version'], 'string', 'max' => 255],
            [['deck_publish'], 'string', 'max' => 1],
        ];

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'deck_faction' => Yii::t('app', 'Deck Faction'),
            'deck_name' => Yii::t('app', 'Deck Name'),
            'deck_img_path' => Yii::t('app', 'Deck Img Path'),
            'deck_version' => Yii::t('app', 'Deck Version'),
            'deck_description' => Yii::t('app', 'Deck Description'),
            'deck_strategy' => Yii::t('app', 'Deck Strategy'),
            'deck_publish' => Yii::t('app', 'Deck Publish'),
        ];
    }
}


