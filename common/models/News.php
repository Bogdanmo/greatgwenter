<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $page_img_path
 * @property string $page_alias
 * @property string $page_title
 * @property string $page_meta_d
 * @property string $page_meta_k
 * @property string $page_content
 * @property string $page_date
 * @property string $page_publish
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_title', 'page_meta_d', 'page_content', 'page_date'], 'required'],
            [['page_meta_d', 'page_meta_k', 'page_content'], 'string'],
            [['page_img_path', 'page_alias', 'page_title', 'page_date'], 'string', 'max' => 255],
            [['page_publish'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'page_img_path' => Yii::t('app', 'Page Img Path'),
            'page_alias' => Yii::t('app', 'Page Alias'),
            'page_title' => Yii::t('app', 'Page Title'),
            'page_meta_d' => Yii::t('app', 'Page Meta D'),
            'page_meta_k' => Yii::t('app', 'Page Meta K'),
            'page_content' => Yii::t('app', 'Page Content'),
            'page_date' => Yii::t('app', 'Page Date'),
            'page_publish' => Yii::t('app', 'Page Publish'),
        ];
    }
}
