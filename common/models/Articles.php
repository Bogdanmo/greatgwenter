<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "articles".
 *
 * @property integer $id
 * @property string $page_img_path
 * @property string $page_alias
 * @property string $page_category
 * @property string $page_title
 * @property string $page_meta_d
 * @property string $page_meta_k
 * @property string $page_content
 * @property string $page_publish
 */
class Articles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'articles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_title', 'page_meta_d', 'page_content'], 'required'],
            [['page_meta_d', 'page_meta_k', 'page_content'], 'string'],
            [['page_img_path', 'page_alias', 'page_category', 'page_title'], 'string', 'max' => 255],
            [['page_publish'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'page_img_path' => Yii::t('app', 'Page Img Path'),
            'page_alias' => Yii::t('app', 'Page Alias'),
            'page_category' => Yii::t('app', 'Page Category'),
            'page_title' => Yii::t('app', 'Page Title'),
            'page_meta_d' => Yii::t('app', 'Page Meta D'),
            'page_meta_k' => Yii::t('app', 'Page Meta K'),
            'page_content' => Yii::t('app', 'Page Content'),
            'page_publish' => Yii::t('app', 'Page Publish'),
        ];
    }
}
class UploadForm extends \yii\db\ActiveRecord
    {
    /**
     * @var UploadedFile
     */
    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->imageFile->saveAs('uploads/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            return true;
        } else {
            return false;
        }
    }
}
