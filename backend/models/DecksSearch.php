<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\decks;

/**
 * DecksSearch represents the model behind the search form about `common\models\decks`.
 */
class DecksSearch extends decks
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['deck_faction', 'deck_name', 'deck_img_path', 'deck_version', 'deck_description', 'deck_strategy', 'deck_publish'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = decks::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'deck_faction', $this->deck_faction])
            ->andFilterWhere(['like', 'deck_name', $this->deck_name])
            ->andFilterWhere(['like', 'deck_img_path', $this->deck_img_path])
            ->andFilterWhere(['like', 'deck_version', $this->deck_version])
            ->andFilterWhere(['like', 'deck_description', $this->deck_description])
            ->andFilterWhere(['like', 'deck_strategy', $this->deck_strategy])
            ->andFilterWhere(['like', 'deck_publish', $this->deck_publish]);

        return $dataProvider;
    }
}
