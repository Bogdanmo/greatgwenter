<?php
use yii\widgets\ActiveForm;
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

<?= $form->field($model, 'imageFile')->fileInput();?>

    <button>Submit</button>

<?php ActiveForm::end() ?>
<h3>Загруженные файлы</h3>
<?php echo readfile("files/uploaded_images.txt"); ?>
