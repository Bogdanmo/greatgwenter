<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ArticlesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="articles-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'page_img_path') ?>

    <?= $form->field($model, 'page_alias') ?>

    <?= $form->field($model, 'page_category') ?>

    <?= $form->field($model, 'page_title') ?>

    <?php // echo $form->field($model, 'page_meta_d') ?>

    <?php // echo $form->field($model, 'page_meta_k') ?>

    <?php // echo $form->field($model, 'page_content') ?>

    <?php // echo $form->field($model, 'page_publish') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
