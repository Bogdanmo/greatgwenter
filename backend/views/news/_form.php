<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\news */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'page_img_path')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'page_alias')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'page_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'page_meta_d')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'page_meta_k')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'page_content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'page_date')->textInput(['maxlength' => true, 'value' => date("d.m.y")]) ?>

    <?= $form->field($model, 'page_publish')->textInput(['maxlength' => true, 'value' => 'Y']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
