<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\decks */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="decks-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'deck_faction')->textInput(['maxlength' => true])->dropDownList([
        'northernrealms' => 'Королевства Севера',
        'skellige' => 'Скеллиге',
        'monsters' =>'Чудовища',
        'scoiatael' => "Скоя'таэли",
        'nilfgaard' => 'Нильфгаард'
        ]); ?>

    <?= $form->field($model, 'deck_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deck_img_path')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deck_version')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deck_description')->textarea(['rows' => 10]) ?>

    <?= $form->field($model, 'deck_strategy')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'deck_publish')->textInput(['maxlength' => true, 'value' => 'Y']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
