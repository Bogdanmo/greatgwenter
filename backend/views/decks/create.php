<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\decks */

$this->title = Yii::t('app', 'Create Decks');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Decks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="decks-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
