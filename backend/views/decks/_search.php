<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\DecksSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="decks-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'deck_faction') ?>

    <?= $form->field($model, 'deck_name') ?>

    <?= $form->field($model, 'deck_img_path') ?>

    <?= $form->field($model, 'deck_version') ?>

    <?php // echo $form->field($model, 'deck_description') ?>

    <?php // echo $form->field($model, 'deck_strategy') ?>

    <?php // echo $form->field($model, 'deck_publish') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
