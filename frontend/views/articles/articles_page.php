<?php
use yii\helpers\Html;
use yii\helpers\Url;

use common\models\Articles;
$post = Articles::find()->where(['id' => "$id"])->one();
$this->title = 'Великий Гвинтер | '.$post->page_title;
    ?>
<div class="row">
    <div class="col-md-9">
        <div class="top-panel">
            <?= Html::img(Url::to('/frontend/web/img/Articles_img.png'), ['alt'=>'picture', 'class'=>'top-panel'])?>
            <h1><?=Html::encode($post->page_title)?></h1>
        </div>
    </div>
</div>
        <div id="substrate" class="col-md-9">
                <?php if($post->page_publish <> 'Y') throw new \yii\web\NotFoundHttpException('Данной страницы не существует');
                else { ?>
                <div class="row">
                    <div class="container" style="padding: 50px 20px 50px 0px; line-height: 30px ">
                        <div class="col-md-9">
                            <p align="justify"><?= $post->page_content?></p>
                            <?php if (isset($post->page_img_path)) { ?>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-lg-12" style="">
                                    <?php if(!$post->page_img_path == "") { echo Html::img(Url::to('/backend/web/uploads/' . $post->page_img_path, true), ['alt'=>'picture', 'class'=>'img-thumbnail deck_img']); }?>
                                </div>
                            </div>
                                <?php } ?>
                        </div>
                    </div>
                </div>
        </div>
<?}?>
<!--HTMLPurifier::process(
Yii::$app->formatter->asntext(Html::encode())-->
