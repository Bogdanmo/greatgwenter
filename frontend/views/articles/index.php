<?php
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use common\models\Articles;
/* @var $this yii\web\View */
$this->title = 'Великий Гвинтер | Статьи';
?>
<div id="substrate" class="col-md-9">
        <h1>Статьи</h1>
        <div class="row">
            <div class="container" style="padding: 50px 30px 50px 0px;">
            <div class="col-md-3">
            <div class="panel panel-success">
                <div class="panel-heading">Основы</div>
                <div class="panel-body">
                    <?php
                    $dataProvider = new ActiveDataProvider([
                        'query' => Articles::find()->where(['page_publish'=>'Y', 'page_category'=>'learning'])->orderBy('id'),
                        'pagination' => [
                            'pageSize' => 30,
                        ],
                    ]);

                    echo ListView::widget([
                        'dataProvider' => $dataProvider,
                        'itemView' => '_list',

                        'options' => [
                            'tag' => 'div',
                            'class' => 'decks-list',
                            'id' => 'decks-list',
                        ],


                        //'layout' => "{pager}\n{summary}\n{items}\n{pager} Показано {count} из {totalCount}",
                        'summary' => '',
                        'summaryOptions' => [
                            'tag' => 'span',
                            'class' => 'my-summary'
                        ],

                        'itemOptions' => [
                            'tag' => 'div',
                            'class' => 'news-item',
                        ],

                        'emptyText' => '<p>Список пуст</p>',
                        'emptyTextOptions' => [
                            'tag' => 'p'
                        ],

    //                'pager' => [
    //                    //'firstPageLabel' => 'Первая',
    //                    //'lastPageLabel' => 'Последняя',
    //                    'nextPageLabel' => 'Следующая',
    //                    'prevPageLabel' => 'Предыдущая',
    //                    'maxButtonCount' => 3,
    //                ],
                    ]);
                    ?>
                    </div>
                 </div>
            </div>
                <div class="col-md-3">
                    <div class="panel panel-success">
                        <div class="panel-heading">Тонкости игры</div>
                        <div class="panel-body">
                            <?php
                            $dataProvider = new ActiveDataProvider([
                                'query' => Articles::find()->where(['page_publish'=>'Y', 'page_category'=>'advanced'])->orderBy('id DESC'),
                                'pagination' => [
                                    'pageSize' => 30,
                                ],
                            ]);

                            echo ListView::widget([
                                'dataProvider' => $dataProvider,
                                'itemView' => '_list',

                                'options' => [
                                    'tag' => 'div',
                                    'class' => 'decks-list',
                                    'id' => 'decks-list',
                                ],


                                //'layout' => "{pager}\n{summary}\n{items}\n{pager} Показано {count} из {totalCount}",
                                'summary' => '',
                                'summaryOptions' => [
                                    'tag' => 'span',
                                    'class' => 'my-summary'
                                ],

                                'itemOptions' => [
                                    'tag' => 'div',
                                    'class' => 'news-item',
                                ],

                                'emptyText' => '<p>Список пуст</p>',
                                'emptyTextOptions' => [
                                    'tag' => 'p'
                                ],

    //                'pager' => [
    //                    //'firstPageLabel' => 'Первая',
    //                    //'lastPageLabel' => 'Последняя',
    //                    'nextPageLabel' => 'Следующая',
    //                    'prevPageLabel' => 'Предыдущая',
    //                    'maxButtonCount' => 3,
    //                ],
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
                    <div class="col-md-3">
                        <div class="panel panel-success">
                            <div class="panel-heading">Остальное</div>
                            <div class="panel-body">
                                <?php
                                $dataProvider = new ActiveDataProvider([
                                    'query' => Articles::find()->where(['page_publish'=>'Y', 'page_category'=>'other'])->orderBy('id DESC'),
                                    'pagination' => [
                                        'pageSize' => 30,
                                    ],
                                ]);

                                echo ListView::widget([
                                    'dataProvider' => $dataProvider,
                                    'itemView' => '_list',

                                    'options' => [
                                        'tag' => 'div',
                                        'class' => 'decks-list',
                                        'id' => 'decks-list',
                                    ],


                                    //'layout' => "{pager}\n{summary}\n{items}\n{pager} Показано {count} из {totalCount}",
                                    'summary' => '',
                                    'summaryOptions' => [
                                        'tag' => 'span',
                                        'class' => 'my-summary'
                                    ],

                                    'itemOptions' => [
                                        'tag' => 'div',
                                        'class' => 'news-item',
                                    ],

                                    'emptyText' => '<p>Список пуст</p>',
                                    'emptyTextOptions' => [
                                        'tag' => 'p'
                                    ],

    //                'pager' => [
    //                    //'firstPageLabel' => 'Первая',
    //                    //'lastPageLabel' => 'Последняя',
    //                    'nextPageLabel' => 'Следующая',
    //                    'prevPageLabel' => 'Предыдущая',
    //                    'maxButtonCount' => 3,
    //                ],
                                ]);
                                ?>
                            </div>
                        </div>
                    </div>
            </div>
    </div>
</div>
