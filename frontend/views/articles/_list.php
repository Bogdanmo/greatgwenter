<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
?>

<div class="articles-item">
    <a href="<?= Url::to(['articles/articles-page', 'id' => $model->id, 'alias' => $model->page_alias]);?>" style="text-decoration: none; color: green">
        <h5><?= Html::encode($model->page_title) ?></h5></a>
<!--    <p class="pull-right">--><?//= Html::encode($model->page_date)?><!--</p>-->
</div>