<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => '<img id="brand-logo" style="height: 150%" src="'.\Yii::$app->request->BaseUrl.'/img/brand.png"/>',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-default navbar-fixed-top',
			'id' => 'main-menu',
        ],
    ]);
    $menuItems = [
        ['label' => 'Статьи', 'url' => ['/articles']],
        [
            'label' => 'Колоды',
            'url' => ['decks'],
            'items' => [
                ['label' => 'Королевства Севера', 'url' => Url::to(['/decks', 'name' => 'northernrealms'])],
                '<li class="divider"></li>',
                ['label' => 'Скеллиге', 'url' => Url::to(['/decks', 'name' => 'skellige'])],
                '<li class="divider"></li>',
                ['label' => 'Чудовища', 'url' => Url::to(['/decks', 'name' => 'monsters'])],
                '<li class="divider"></li>',
                ['label' => "Скоя'таэли", 'url' => Url::to(['/decks', 'name' => 'scoiatael'])],
                '<li class="divider"></li>',
                ['label' => 'Нильфгаард', 'url' => Url::to(['/decks', 'name' => 'nilfgaard'])],

            ],
        ],
		['label' => 'Новости', 'url' => ['/news']],
    ];
    /*if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }*/
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);?>
                    <?= Alert::widget() ?>
                    <?php echo $content ?>
            </div>
    </div>
<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Великий Гвинтер, <?= date('Y') ?></p>

    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
