<?php
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
use common\models\Decks;

$post = Decks::find()->where(['id' => "$id"])->one();
$this->title = 'Великий Гвинтер | '.$post->deck_name;
if($post->deck_publish <> 'Y') throw new \yii\web\NotFoundHttpException('Данной страницы не существует');
else {
?>
    <div class="block">
        <div id="substrate" class="col-md-9">
            <div class="">
                <h1 style="text-align: center"><?=Html::encode($post->deck_name)?></h1>
            </div>
            <div class="row">
                <div class="col-md-12" style="">
                    <?= Html::img(Url::to('/backend/web/uploads/' . $post->deck_img_path, true), ['alt'=>'Deck picture', 'class'=>'img-thumbnail deck_img'])?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <div class="container" style="padding: 20px 50px 20px 50px">
                        <p><?= $post->deck_strategy?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="block">
        <div id="substrate_label" class="col-md-3 col-sm-12">
            <div class="row">
                <h3 style="text-align: center">Состав колоды:</h3>
                <div style="border: solid 1px black; border-radius: 5px; margin: 3px; text-align: center">
                    <p><?=$post->deck_description?></p>
                </div>
            </div>
        </div>
    </div>

<?}?>