<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;

?>

<div class="decks-item" style="">
        <img id="faction_icon" class="pull-left" style="height: 80%; border-right: black 1px solid; margin: 3px" src="<?= \Yii::$app->request->baseUrl?>/img/<?= $model->deck_faction?>_icon.png"/>
    <div style="">
        <a href="<?= Url::to(['decks/decks-page', 'id' => $model->id])?>" style="text-decoration: none; color: green"><h3><?= Html::encode($model->deck_name) ?>(<?= Html::encode($model->deck_version) ?>)</h3></a>
    </div>
</div>
