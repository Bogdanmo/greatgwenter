<?php
use common\models\decks;
//use yii\helpers\Url;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
$this->title = 'Великий Гвинтер | Колоды';
/* @var $this yii\web\View */
?>
<div id="substrate" class="col-md-9">
<div class="container">
    <h1>Колоды</h1>
<?php
switch ($name) {
    case 'northernrealms':
        echo "<h3>Королевства Севера</h3>";
        $icon_path = '/img/icon_northernrealms.png';
        break;
    case 'skellige':
        echo "<h3>Скеллиге</h3>";
        $icon_path = '/img/icon_skellige.png';
        break;
    case 'monsters':
        echo "<h3>Чудовища</h3>";
        $icon_path = '/img/icon_monsters.png';
        break;
    case 'scoiatael':
        echo "<h3>Скоя'таэли</h3>";
        $icon_path = '/img/icon_scoiatael.png';
        break;
    case 'nilfgaard':
        echo "<h3>Нильфгаард</h3>";
        $icon_path = '/img/icon_nilfgaard.png';
        break;
    default:
        throw new \yii\web\NotFoundHttpException();
}
    ?>
        <div class="row">
            <div class="container">
            <div class="col-md-8">
            <div class="content_sector"><?php
    $dataProvider = new ActiveDataProvider([
        'query' => Decks::find()->where(['deck_publish'=>'Y', 'deck_faction'=>$name])->orderBy('id DESC'),
        'pagination' => [
            'pageSize' => 20,
        ],
    ]);

    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_list',

        'options' => [
            'tag' => 'div',
            'class' => 'decks-list',
            'id' => 'decks-list',
        ],


        //'layout' => "{pager}\n{summary}\n{items}\n{pager} Показано {count} из {totalCount}",
        'summary' => '',
        'summaryOptions' => [
            'tag' => 'span',
            'class' => 'my-summary'
        ],

        'itemOptions' => [
            'tag' => 'div',
            'class' => 'news-item',
        ],

        'emptyText' => '<p>Список пуст</p>',
        'emptyTextOptions' => [
            'tag' => 'p'
        ],

        'pager' => [
            //'firstPageLabel' => 'Первая',
            //'lastPageLabel' => 'Последняя',
            'nextPageLabel' => 'Следующая',
            'prevPageLabel' => 'Предыдущая',
            'maxButtonCount' => 3,
        ],
    ]);
    ?>
                     </div>
                 </div>
            </div>
        </div>
    </div>
</div>

