<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
?>

<div class="news-item">
    <a href="<?= Url::to(['news/news-page', 'id' => $model->id]);?>" style="text-decoration: none; color: green"><h3><?= Html::encode($model->page_title) ?></h3></a>
    <?= HtmlPurifier::process($model->page_meta_d) ?>
    <p class="pull-right"><?= Html::encode($model->page_date)?></p>
</div>