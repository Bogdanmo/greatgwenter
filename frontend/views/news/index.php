<?php
/* @var $this yii\web\View */
use common\models\news;
//use yii\helpers\Url;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
$this->title = 'Великий Гвинтер | Новости';
?>
<div id="substrate" class="col-md-9">
    <div class="container">
        <h1>Новости</h1>
        <div class="row">
            <div class="container">
                <div class="col-md-8">
                    <div class="content_sector">
                     <?php
                    $dataProvider = new ActiveDataProvider([
                        'query' => News::find()->where(['page_publish'=>'Y'])->orderBy('id DESC'),
                        'pagination' => [
                            'pageSize' => 20,
                        ],
                    ]);

                    echo ListView::widget([
                        'dataProvider' => $dataProvider,
                        'itemView' => '_list',

                        'options' => [
                            'tag' => 'div',
                            'class' => 'news-list',
                            'id' => 'news-list',
                        ],


                        //'layout' => "{pager}\n{summary}\n{items}\n{pager} Показано {count} из {totalCount}",
                        'summary' => '',
                        'summaryOptions' => [
                            'tag' => 'span',
                            'class' => 'my-summary'
                        ],

                        'itemOptions' => [
                            'tag' => 'div',
                            'class' => 'news-item',
                        ],

                        'emptyText' => '<p>Список пуст</p>',
                        'emptyTextOptions' => [
                            'tag' => 'p'
                        ],

                        'pager' => [
                            //'firstPageLabel' => 'Первая',
                            //'lastPageLabel' => 'Последняя',
                            'nextPageLabel' => 'Следующая',
                            'prevPageLabel' => 'Предыдущая',
                            'maxButtonCount' => 3,
                        ],
                    ]);?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>