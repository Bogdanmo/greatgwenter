<?php
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
//use yii\helpers\HtmlPurifier;
use common\models\News;

$post = News::find()->where(['id' => "$id"])->one();
$this->title = 'Великий Гвинтер | '.$post->page_title;
if($post->page_publish <> 'Y') throw new \yii\web\NotFoundHttpException('Данной страницы не существует');
else {
?>
<div id="substrate" class="col-md-9">
    <div class="row">
        <div class="container" style="padding: 20px 50px 20px 50px; line-height: 30px ">
            <div class="col-md-9">
                <h1><?=Html::encode($post->page_title)?></h1>
                <p><?=$post->page_content?></p>
                <p><?=Html::encode($post->page_date)?></p>
            </div>
        </div>
    </div>
</div>
<?}?>
<!--Yii::$app->formatter->asntext(Html::encode())-->
