<?php

namespace frontend\controllers;

class DecksController extends \yii\web\Controller
{
    public function actionIndex($name)
    {
        return $this->render('index', ['name' => $name]);
    }
    public function actionDecksPage($id)
    {
        return $this->render('decks_page', ['id' => $id]);
    }

}
