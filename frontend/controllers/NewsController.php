<?php

namespace frontend\controllers;

use yii\web\NotFoundHttpException;

class NewsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionNewsPage($id)
    {
        return $this->render('news_page', ['id' => $id]);
    }
}
