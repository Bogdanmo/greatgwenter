<?php

namespace frontend\controllers;

class ArticlesController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionArticlesPage($alias, $id)
    {
        return $this->render('articles_page', ['alias' => $alias, 'id' => $id]);
    }
}
